Name: valadoc
Version: 0.30
Release: 1%{?dist}
Summary: Vala documentation generator

License: GPLv2+
URL: https://wiki.gnome.org/Projects/Valadoc
Source: https://git.gnome.org/browse/valadoc/snapshot/valadoc-valac-%{version}.tar.xz

BuildRequires: pkgconfig
BuildRequires: glib2-devel
BuildRequires: gettext
BuildRequires: automake autoconf libtool
BuildRequires: gtk-doc
BuildRequires: graphviz-devel
BuildRequires: libgee-devel
BuildRequires: vala
BuildRequires: vala-devel
Requires: glib2
Requires: graphviz
Requires: vala
Requires: libgee

%description
Valadoc is a documentation generator for Vala projects. It extracts
source code from Vala source files and can output various formats of
documentation like GTK-Doc or GIR documentation.

Documentation of many common libraries can be found at
http://valadoc.org/

%package devel
Requires: %{name}%{_isa} = %{version}-%{release}
Summary: Vala documentation generator

%description devel
Development files for Valadoc.

%prep
%autosetup -n valadoc-valac-%{version}

%build
./autogen.sh
%configure
%make_build

%install
%make_install
rm -f %{buildroot}/%{_libdir}/*.la
rm -f %{buildroot}/%{_libdir}/valadoc/doclets/*/*.la
rm -f %{buildroot}/%{_libdir}/valadoc/drivers/*/*.la
rm -f %{buildroot}/%{_libdir}/*.a
find %{buildroot}/%{_libdir}/valadoc/drivers/ -type f \! -path *%{version}* -delete

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%license COPYING
%doc ChangeLog
%{_bindir}/valadoc
%{_libdir}/libvaladoc.so.*
%{_libdir}/%{name}/doclets/devhelp/libdoclet.so
%{_libdir}/%{name}/doclets/gtkdoc/libdoclet.so
%{_libdir}/%{name}/doclets/html/libdoclet.so
%{_libdir}/%{name}/drivers/0.30.x/libdriver.so
%{_mandir}/man1/valadoc.1.gz
%{_datadir}/%{name}/icons/*.png
%{_datadir}/%{name}/icons/*.css
%{_datadir}/%{name}/icons/*.js

%files devel
%{_includedir}/*
%{_libdir}/libvaladoc.so
%{_libdir}/pkgconfig/valadoc-1.0.pc
%{_datadir}/vala/vapi/valadoc-1.0.deps
%{_datadir}/vala/vapi/valadoc-1.0.vapi

%changelog
* Mon Apr 18 2016 Gergely Polonkai <gergely@polonkai.eu> 0.30-1
- Apply recommended modifications from #988667
- Fix require tag in -devel

* Wed Apr 13 2016 Gergely Polonkai <gergely@polonkai.eu>
- Add recommended modifications from #988667

* Tue Apr 5 2016 Gergely Polonkai <gergely@polonkai.eu>
- Apply recommended modifications from #988667

* Thu Mar 31 2016 Gergely Polonkai <gergely@polonkai.eu>
- Apply recommended modifications from #988667

* Tue Feb 23 2016 Gergely Polonkai <gergely@polonkai.eu>
- Initial version
